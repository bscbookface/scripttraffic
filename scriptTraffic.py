#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Script for generating traffic to rabbitmq service architecture
"""

from random import randint
import os
import getopt
import sys
import urllib2
import urllib
import json
import requests

"""Options:
 U = URL REQUIRED OPTION
 O = One new entry
 M = Many new entries

Example: 
>>> ./scriptTraffic.py -U "http://192.168.200.129:5000/job" -M 2
{
    "jobType" : 5,
    "name": "Sam Neal", 
    "stamp": [], 
    "timeStart": "Mon Apr 25 16:14:27 2016"
}

{
    "jobType" : 2,
    "name": "Beverly Parker", 
    "stamp": [], 
    "timeStart": "Mon Apr 25 16:14:27 2016"
}

JobType can represent anything. For example 1 can be a car, 2 can be a plane
and so forth.

"""

# CONSTANTS
TEMPDIR = "/run/shm/"
TRACEBACK = 1

# Checking if URL (-U) is passed as argument
FOUND_OPTION_U = False
OPTIONS, REM = getopt.getopt(sys.argv[1:], 'U:OM:F')
for alt, argum in OPTIONS: 
    if alt in '-U':
        URL = argum
        FOUND_OPTION_U = True

    if not FOUND_OPTION_U:
        print "NO URL GIVEN"
        sys.exit()


def get_random_name():
    """ Function for getting a random user name"""
    # Downloads a json document with random user information
    response = urllib2.urlopen('http://api.randomuser.me/1.0/?nat=gb,us&inc=name&noinfo').read()
    parsed_json = json.loads(response)  # Parse the json into variable
    firstname = parsed_json["results"][0]["name"]["first"].title()
    # Select firstname and capitalize
    lastname = parsed_json["results"][0]["name"]["last"].title()
    # select Lastname and capitalize

    name = firstname + " " + lastname     # Add names together

    return name     # Return generated name


def front_page():
    """ Function that visits the front page"""
    rand = randint(0, 1000)  # Get random int for folder name

    folder = TEMPDIR + str(rand) # make folder path of tempdir and random int
    os.mkdir(folder)  # Make new folder
    os.chdir(folder)  # Change directory to folder

    os.system("wget -t 2 -T 5 -p -q "+ URL)
    # Wget all elements from webpage inc css,logo,photos
    os.system("rm -r "+ folder)  # Remove folder and its contents

    print "visited front page"


def new_entry():
    """ Function to add a single new entry"""
    name = get_random_name()
    jobType = randint(1, 5)
    # Constructing the request
    payload = {'name' : name, 'jobType' : jobType}
    headers = {'Content-Type': 'application/json'}
    # Execute the request with error handling for general and 404
    try:
        req = requests.post(URL, data=json.dumps(payload), headers=headers)
    except requests.exceptions.RequestException as err:
        print_error_msg(err)
        sys.exit(TRACEBACK)
    else:
        if req.status_code == 404:
            print_error_msg(req.raise_for_status())
            sys.exit(TRACEBACK)
    # Print out the reply text
    print req.text
    return req.text

def many_new_entries(number):
    """ Function to add several new jobs, using previous functions"""
    # For number in range, add a new entry
    if number.isdigit():
        for i in range(int(number)):
            new_entry()
    else:
        print_error_msg("Passed variable is not a number. Exiting...")
        sys.exit(TRACEBACK)
    print "Done! Sent %s new entries." % number


def print_error_msg(err):
    """ Simple error print handler """
    print "Error message: %s " % err
    

def main():
    """ Main menu, takes option as command line argument"""
    # Depending on the input, choose an option
    for opt, arg in OPTIONS:
        if opt in '-F':
            front_page()
        elif opt in '-O':
            new_entry()
        elif opt in '-M':
            many_new_entries(arg)


if __name__ == '__main__':
    main()



