# ScriptTraffic

Traffic generator for Factory architecture

## Installation

1. Clone from git

2. cd scripttraffic/

3. pip install -r requirements.txt

## Usage

Options:

* U = URL REQUIRED OPTION

* O = One new entry

* M = Many new entries

Example: 
``` 
>>> ./scriptTraffic.py -U "http://192.168.200.129:5000/job" -M 2

{
    "jobType" : 5,
    "name": "Sam Neal", 
    "stamp": [], 
    "timeStart": "Mon Apr 25 16:14:27 2016"
}

{
    "jobType" : 2,
    "name": "Beverly Parker", 
    "stamp": [], 
    "timeStart": "Mon Apr 25 16:14:27 2016"
}

```

JobType can represent anything. For example 1 can be a car, 2 can be a plane
and so forth. Numbers are just an easy way to represent this.

